import express from "express";
import users from "../controllers/user.controller.js";

const router = express.Router();

// Create a new User
router.post("/", users.create);

// Retrieve all User
router.get("/", users.findAll);

export default router;