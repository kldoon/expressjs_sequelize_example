import { DataTypes } from "sequelize";

const User = (sequelize) => {
  const User = sequelize.define("user", {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING
    },
    age: {
      type: DataTypes.INTEGER
    },
    address: {
      type: DataTypes.STRING
    }
  });

  return User;
};

export default User;