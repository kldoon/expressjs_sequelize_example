import db from "../models/index.js";
import Sequelize from "sequelize";

const Op = Sequelize.Op;

// Create and Save a new users
const create = (req, res) => {
  // Create a User
  const user = {
    name: req.body.name,
    age: req.body.age,
    address: req.body.address
  };

  // Save User in the database
  db.User.create(user)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the User."
      });
    });
};

// Retrieve all users from the database.
const findAll = (req, res) => {
  const name = req.query.name;
  var condition = name ? { name: { [Op.like]: `%${name}%` } } : null;

  db.User.findAll({ where: condition })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving users."
      });
    });

};

export default {
  create,
  findAll
}
