import express from 'express';
import cors from 'cors';
import db from "./models/index.js"
import usersRoute from './routes/user.routes.js'


const port = 3000;
const host = '127.0.0.1';

const app = express();

app.use(cors());
app.use(express.json());

app.use('/api/users', usersRoute);


db.sequelize.sync()
  .then(() => {
    console.log("Synced db.");
  })
  .catch((err) => {
    console.log("Failed to sync db: " + err.message);
  });

app.get('/users', () => {
  
})

app.post('/users', () => {
 
});

app.listen(port, host, () => {
  console.log(`Server is listening on http://${host}:${port}`)
});